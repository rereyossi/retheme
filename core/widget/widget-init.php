<?php
namespace Retheme;

class Widget_Init
{

    public function __construct()
    {

        add_action('widgets_init', array($this, 'register_widget_location'));
        add_action('widgets_init', array($this, 'register_widget_element'));

    }

    public function register_widget_location()
    {

        register_sidebar(array(
            'name' => __('Sidebar', 'rt_domain'),
            'id' => 'retheme_sidebar',
            'before_widget' => '<div id ="%1$s" class="rt-widget rt-widget--aside %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<div class="rt-widget__header"><h3 class="rt-widget__title">',
            'after_title' => '</h3></div>',
        ));

        if (class_exists('woocommerce')) {

            register_sidebar(array(
                'name' => __('WooCommerce Sidebar', 'rt_domain'),
                'id' => 'retheme_woocommerce_sidebar',
                'before_widget' => '<div id ="%1$s" class="rt-widget rt-widget--aside %2$s">',
                'after_widget' => '</div>',
                'before_title' => '<div class="rt-widget__header"><h3 class="rt-widget__title">',
                'after_title' => '</h3></div>',
            ));
        }

        register_sidebar(array(
            'name' => __('Footer Column 1', 'rt_domain'),
            'id' => 'retheme_footer_1',
            'before_widget' => '<div id ="%1$s" class="rt-widget rt-widget--footer %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<div class="rt-widget__header"><h3 class="rt-widget__title">',
            'after_title' => '</h3></div>',
        ));

        register_sidebar(array(
            'name' => __('Footer Column 2', 'rt_domain'),
            'id' => 'retheme_footer_2',
            'before_widget' => '<div id ="%1$s" class="rt-widget rt-widget--footer %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<div class="rt-widget__header"><h3 class="rt-widget__title">',
            'after_title' => '</h3></div>',
        ));

        register_sidebar(array(
            'name' => __('Footer Column 3', 'rt_domain'),
            'id' => 'retheme_footer_3',
            'before_widget' => '<div id ="%1$s" class="rt-widget rt-widget--footer %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<div class="rt-widget__header"><h3 class="rt-widget__title">',
            'after_title' => '</h3></div>',
        ));

        register_sidebar(array(
            'name' => __('Footer Column 4', 'rt_domain'),
            'id' => 'retheme_footer_4',
            'before_widget' => '<div id ="%1$s" class="rt-widget rt-widget--footer %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<div class="rt-widget__header"><h3 class="rt-widget__title">',
            'after_title' => '</h3></div>',
        ));

        register_sidebar(array(
            'name' => __('Footer Column 5', 'rt_domain'),
            'id' => 'retheme_footer_5',
            'before_widget' => '<div id ="%1$s" class="rt-widget rt-widget--footer %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<div class="rt-widget__header"><h3 class="rt-widget__title">',
            'after_title' => '</h3></div>',
        ));

        register_sidebar(array(
            'name' => __('Footer Column 6', 'rt_domain'),
            'id' => 'retheme_footer_6',
            'before_widget' => '<div id ="%1$s" class="rt-widget rt-widget--footer %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<div class="rt-widget__header"><h3 class="rt-widget__title">',
            'after_title' => '</h3></div>',
        ));

    }

    public function register_widget_element()
    {

        if (rt_is_premium()) {
            include dirname(__FILE__) . '/author/widget-author.php';
            include dirname(__FILE__) . '/posts/widget-posts.php';
            include dirname(__FILE__) . '/posts-tab/widget-posts-tabs.php';
            include dirname(__FILE__) . '/social/widget-social.php';
            include dirname(__FILE__) . '/company/widget-company.php';

            register_widget('Retheme\Widget\Widget_Author');
            register_widget('Retheme\Widget\Widget_Posts');
            register_widget('Retheme\Widget\Widget_Post_Tabs');
            register_widget('Retheme\Widget\Widget_Sosmed');
            register_widget('Retheme\Widget\Widget_Company');

        }

    }

}

new Widget_Init;
