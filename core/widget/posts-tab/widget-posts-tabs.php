<?php
namespace Retheme\Widget;

class Widget_Post_Tabs extends \WP_Widget
{
    public function __construct()
    {
        $args = array(
            'classname' => 'retheme-widget-post-tab',
        );

        parent::__construct('retheme_widget_post_tabs', 'Retheme - Post Tabs', $args);

    }

    // VIEW
    public function widget($args, $instance)
    {
        // outputs the content of the widget
        if (!isset($args['widget_id'])) {
            $args['widget_id'] = $this->id;
        }

        // widget ID with prefix for use in ACF API functions
        $widget_id = 'widget_' . $args['widget_id'];

        $title = rt_get_field('title', $widget_id);

        echo $args['before_widget'];

        if ($title) {
            echo $args['before_title'] . esc_html($title) . $args['after_title'];
        }

        $popular = rt_get_field('popular', $widget_id);
        $most_comment = rt_get_field('most_comment', $widget_id);
        $lastest = rt_get_field('lastest', $widget_id);
        $post_per_page = rt_get_field('posts_per_page', $widget_id);

        //popular
        $query_popular = array(
            'posts_per_page' => $post_per_page,
            'post_type' => 'post',
            'meta_key' => 'wp_post_views_count',
            'orderby' => 'meta_value_num',
            'order' => 'DESC',
            'post_status' => 'publish',
        );

        //Newer
        $query_lastest = array(
            'posts_per_page' => $post_per_page,
            'post_type' => 'post',
            'post_status' => 'publish',
        );

        //Most Comment
        $query_most_comment = array(
            'posts_per_page' => $post_per_page,
            'post_type' => 'post',
            'post_status' => 'publish',
            'stats_comments' => 1,
            'order_by' => 'comment_count',
        );

        include dirname(__FILE__) . '/widget-posts-tabs-view.php';

        echo $args['after_widget'];
    }

    // BACKEND
    public function form($instance)
    {

    }

    // UPDATE
    public function update($new_instance, $old_instance)
    {

    }
}
