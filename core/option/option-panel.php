<?php 
if (function_exists('acf_add_local_field_group')):

    acf_add_local_field_group(array(
        'key' => 'group_5bb224cc4e793',
        'title' => 'Theme Panel',
        'fields' => array(
            array(
                'key' => 'field_5bd819f59bf3d',
                'label' => 'Google Analytic',
                'name' => 'google_analytic',
                'type' => 'text',
                'instructions' => 'Add your Google Analytic ID. Remove id if you use another method or plugin',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_5bd819a29bf3c',
                'label' => 'Facebook Pixels',
                'name' => 'facebook_pixel',
                'type' => 'text',
                'instructions' => 'Add your Facebook Pixel ID. Remove id if you use another method or plugin',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'theme-options',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
    ));

endif;
