<?php
namespace Retheme;

class Activation
{

    public $secret_key = '5c0a2dd7949698.97710041s';
    public $server_host = 'https://webforia.id/';

    public function __construct()
    {
        add_action('admin_notices', array($this, 'theme_notice'));
        add_action('admin_notices', array($this, 'check_license'));

        add_action("admin_init", array($this, 'check_license'));
    }

    public function check_license()
    {

        if (!empty($_POST['theme_license_submit']) && $_POST['theme_license_submit'] == 'Activate License') {
            $this->activation($_POST['theme_license_key']);
        }

        if (!empty($_POST['theme_license_submit']) && $_POST['theme_license_submit'] == 'Deactivate License') {
            $this->deactivation($_POST['theme_license_key']);
        }

    }

    public function activation($key)
    {

        $api_params = array(
            'slm_action' => 'slm_activate',
            'secret_key' => $this->secret_key,
            'license_key' => $key,
            'registered_domain' => str_replace(array('www.', 'http://', 'https://'), '', site_url()),
        );

        // Send query to the license manager server
        $query = esc_url_raw(add_query_arg($api_params, $this->server_host));
        $response = wp_remote_get($query, array('timeout' => 20, 'sslverify' => false));

        // Check for error in the response
        if (is_wp_error($response)) {
            echo "Unexpected Error! The query returned with an error.";
        }

        // License data.
        $license_data = json_decode(wp_remote_retrieve_body($response));

        if ($license_data->result == 'success') { //Success was returned for the license activation

            update_option(rt_var('product-slug', '_status'), 'active');
            update_option(rt_var('product-slug', '_key'), $key);

            add_action('admin_notices', function () {
                Notice::get_success_notice($license_data->message);
            });

        } else {
            //Show error to the user. Probably entered incorrect license key.

            update_option(rt_var('product-slug', '_status'), 'deactive');
            update_option(rt_var('product-slug', '_key'), '');

            Notice::get_error_notice($license_data->message);
        }

    }

    public function deactivation($key)
    {

        // API query parameters
        $api_params = array(
            'slm_action' => 'slm_deactivate',
            'secret_key' => $this->secret_key,
            'license_key' => $key,
            'registered_domain' => str_replace(array('www.', 'http://', 'https://'), '', site_url()),
        );

        // Send query to the license manager server
        $query = esc_url_raw(add_query_arg($api_params, $this->server_host));
        $response = wp_remote_get($query, array('timeout' => 20, 'sslverify' => false));

        // Check for error in the response
        if (is_wp_error($response)) {
            echo "Unexpected Error! The query returned with an error.";
        }

        // License data.
        $license_data = json_decode(wp_remote_retrieve_body($response));

        if ($license_data->result == 'success') { //Success was returned for the license activation
            update_option(rt_var('product-slug', '_status'), 'deactive');
            update_option(rt_var('product-slug', '_key'), '');
            //Uncomment the followng line to see the message that returned from the license server
            Notice::get_success_notice($license_data->message);

        } else {
            //Show error to the user. Probably entered incorrect license key.
            Notice::get_error_notice($license_data->message);

        }

    }

    /**
     * Check status on option
     */

    public function get_status()
    {
        return get_option(rt_var('product-slug', '_status'));
    }

    public static function active()
    {
        return (get_option(rt_var('product-slug', '_status')) === 'active') ? true : false;
    }

    /**
     * Show notif
     *
     * @return void
     */
    public function theme_notice()
    {

        // if license success
        if ($this->get_status() == 'active' && get_option(rt_var('product-slug', "-license-dismiss")) != true):
            $success = '<p>' . __("Activation " . rt_var('product-name') . " success", "rt_domain") . '
	                        <span style="display: block; margin: 0.5em  0; clear: both;">
	                            <a href="' . rt_var('product-docs') . '" target="_blank">Visit Tutorial</a>
	                        </span></p>';
            Notice::get_success_notice($success, rt_var('product-slug', "-license-dismiss"));

        endif;
    }

}
new Activation;
