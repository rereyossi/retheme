<?php
/**
 * @author : Reret
 */
namespace Retheme\Customizer;

use Retheme\Customizer_Base;

class Brand extends Customizer_Base
{

    public function __construct()
    {

        $this->add_brand_logo();

    }

    public function add_brand_logo()
    {

        $this->add_header(array(
            'label' => __('Desktop Logo', 'rt_domain'),
            'settings' => 'brand_logo',
            'section' => 'title_tagline',
        ));

        $this->add_field(array(
            'type' => 'image',
            'settings' => 'brand_logo_primary',
            'label' => __('Default Logo', 'rt_domain'),
            'tooltip' => __('This logo will be used as your default logo', 'rt_domain'),
            'section' => 'title_tagline',
        ));

        $this->add_field(array(
            'type' => 'slider',
            'settings' => 'brand_logo_primary_size',
            'label' => __('Default Logo Width', 'rt_domain'),
            'section' => 'title_tagline',
            'choices' => array(
                'min' => '100',
                'max' => '300',
                'step' => '1',
            ),
            'output' => array(
                array(
                    'element' => '.rt-header__main .rt-logo',
                    'property' => 'width',
                    'units' => 'px',
                ),

            ),
            'transport' => 'auto',
        ));

        $this->add_field(array(
            'type' => 'image',
            'settings' => 'brand_logo_sticky',
            'label' => __('Sticky Logo', 'rt_domain'),
            'tooltip' => __('This logo will be used when the header is in a sticky state.', 'rt_domain'),
            'section' => 'title_tagline',
        ));

        $this->add_field(array(
            'type' => 'slider',
            'settings' => 'brand_logo_sticky_size',
            'label' => __('Sticky Logo Width', 'rt_domain'),
            'section' => 'title_tagline',
            'choices' => array(
                'min' => '100',
                'max' => '300',
                'step' => '1',
            ),
            'output' => array(
                array(
                    'element' => '.rt-header__sticky .rt-logo',
                    'property' => 'width',
                    'units' => 'px',
                ),

            ),
            'transport' => 'auto',
        ));

        $this->add_field(array(
            'type' => 'image',
            'settings' => 'brand_logo_overlay',
            'label' => __('Overlay Logo Light', 'rt_domain'),
            'tooltip' => __('This logo will be used when the transparent header is enabled.', 'rt_domain'),
            'section' => 'title_tagline',
        ));

        $this->add_field(array(
            'type' => 'slider',
            'settings' => 'brand_logo_overlay_size',
            'label' => __('Overlay Logo Width', 'rt_domain'),
            'section' => 'title_tagline',
            'choices' => array(
                'min' => '100',
                'max' => '300',
                'step' => '1',
            ),
            'output' => array(
                array(
                    'element' => '.rt-header.is-overlay .rt-header__main .rt-logo',
                    'property' => 'width',
                    'units' => 'px',
                ),

            ),
            'transport' => 'auto',
        ));

        $this->add_header(array(
            'label' => __('Mobile Logo', 'rt_domain'),
            'settings' => 'brand_logo_mobile',
            'section' => 'title_tagline',
        ));

        $this->add_field(array(
            'type' => 'image',
            'settings' => 'brand_logo_mobile',
            'label' => __('Mobile Logo', 'rt_domain'),
            'tooltip' => __('This logo will be used on mobile devices', 'rt_domain'),
            'section' => 'title_tagline',
        ));

        $this->add_field(array(
            'type' => 'slider',
            'settings' => 'brand_logo_mobile_size',
            'label' => __('Mobile Logo Width', 'rt_domain'),
            'section' => 'title_tagline',
            'choices' => array(
                'min' => '80',
                'max' => '200',
                'step' => '1',
            ),
            'output' => array(
                array(
                    'element' => '.rt-header-mobile__main .rt-logo',
                    'property' => 'width',
                    'units' => 'px',
                ),

            ),
            'transport' => 'auto',
        ));

        $this->add_field(array(
            'type' => 'image',
            'settings' => 'brand_logo_mobile_sticky',
            'label' => __('Sticky Logo', 'rt_domain'),
            'tooltip' => __('This logo will be used when the header is in a sticky state on mobile devices.', 'rt_domain'),
            'section' => 'title_tagline',
        ));

        $this->add_field(array(
            'type' => 'slider',
            'settings' => 'brand_logo_mobile_size',
            'label' => __('Mobile Logo Width', 'rt_domain'),
            'section' => 'title_tagline',
            'choices' => array(
                'min' => '80',
                'max' => '200',
                'step' => '1',
            ),
            'output' => array(
                array(
                    'element' => '.rt-header-mobile__main .rt-logo',
                    'property' => 'width',
                    'units' => 'px',
                ),

            ),
            'transport' => 'auto',
        ));

        // $this->add_field(array(
        //     'type' => 'image',
        //     'settings' => 'brand_logo_mobile_overlay',
        //     'label' => __('Overlay Logo Light', 'rt_domain'),
        //     'tooltip' => __('This logo will be used when the transparent header is enabled on mobile devices.', 'rt_domain'),
        //     'section' => 'title_tagline',
        // ));

        // $this->add_field(array(
        //     'type' => 'slider',
        //     'settings' => 'brand_logo_mobile_overlay_size',
        //     'label' => __('Overlay Logo Width', 'rt_domain'),
        //     'section' => 'title_tagline',
        //     'choices' => array(
        //         'min' => '80',
        //         'max' => '200',
        //         'step' => '1',
        //     ),
        //     'output' => array(
        //         array(
        //             'element' => '.rt-header-mobile.is-overlay .rt-header-mobile__main .rt-logo',
        //             'property' => 'width',
        //             'units' => 'px',
        //         ),

        //     ),
        //     'transport' => 'auto',
        // ));

         $this->add_header(array(
            'label' => __('Checkout Logo', 'rt_domain'),
            'settings' => 'focus_logo',
            'section' => 'title_tagline',
        ));

        $this->add_field(array(
            'type' => 'image',
            'settings' => 'focus_logo_primary',
            'label' => __('Checkout Logo', 'rt_domain'),
            'tooltip' => __('This logo will be used on checkout template', 'rt_domain'),
            'section' => 'title_tagline',
        ));

        $this->add_field(array(
            'type' => 'slider',
            'settings' => 'focus_logo_primary_size',
            'label' => __('Checkout Logo Width', 'rt_domain'),
            'section' => 'title_tagline',
            'choices' => array(
                'min' => '100',
                'max' => '300',
                'step' => '1',
            ),
            'output' => array(
                array(
                    'element' => '.ptf-header .rt-logo',
                    'property' => 'width',
                    'units' => 'px',
                ),

            ),
            'transport' => 'auto',
        ));



        $this->add_header(array(
            'label' => __('Pavicon', 'rt_domain'),
            'settings' => 'brand_pavicon',
            'section' => 'title_tagline',
        ));

    }

// end class
}

new Brand;
