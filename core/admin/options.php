<?php

namespace Retheme\Admin;

class Options
{
    public $theme_name = 'Retheme';

    public function __construct()
    {
        add_action('admin_menu', array($this, 'add_menu_panel'));
        add_action('after_setup_theme', array($this, 'add_theme_panel'));
        add_action('admin_post', array($this, 'add_license_panel'));
        // add_action("admin_init", array($this, 'display_fields'));

    }

    public function add_menu_panel()
    {
        add_menu_page('Theme Panel', 'Theme Panel', 'manage_options', 'theme-panel', false);

        add_submenu_page('theme-panel', 'Dashboard', 'Dashboard', 'manage_options', 'theme-panel', function () {
            get_template_part('core/admin/views/dashboard');
        }, 1);

        add_submenu_page('theme-panel', 'License', 'License', 'manage_options', 'theme-license', function () {
            get_template_part('core/admin/views/license');
        }, 99);

    }

    /**
     * Added ACF option panel
     *
     * @return void
     */
    public function add_theme_panel()
    {
        if (function_exists('acf_add_options_page')) {

            acf_add_options_page(array(
                'page_title' => 'Options',
                'menu_title' => 'Options',
                'menu_slug' => 'theme-options',
                'parent_slug' => 'theme-panel',
                'position' => 1,
                'capability' => 'edit_posts',
                'redirect' => false,
            ));
        }

    }

    /**
     * Display license field
     *
     * @return void
     */
    public function display_fields()
    {
       
        
        add_settings_field(rt_var('product-slug', '_email'), "Email", function () {
            echo '<input type="email" name="' . rt_var('product-slug', '_email') . '" id="' . rt_var('product-slug', '_email') . '" value="' . get_option(rt_var('product-slug', '_email')) . '"/>';
        }, "retheme_section", "section", ['disabled' => true]);

        add_settings_field(rt_var('product-slug', '_key'), "Key", function () {
            echo '<input type="text" name="' . rt_var('product-slug', '_key'). '" id="' . rt_var('product-slug', '_key') . '" value="' . get_option(rt_var('product-slug', '_key')) . '"/>';
        }, "retheme_section", "section", ['disabled' => true]);


        register_setting("section", rt_var('product-slug', '_email'));
        register_setting("section", rt_var('product-slug', '_key'));

        add_settings_section("section", "License", null, "retheme_section");

    }

}

new Options;
