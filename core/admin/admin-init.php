<?php
namespace Retheme\Admin;

class Admin
{
    public function __construct()
    {
        if(is_admin()){
            $this->include_part();
            add_action('admin_enqueue_scripts', array($this, 'register_scripts'));
        }

    }

    public function include_part()
    {
        include_once dirname(__FILE__) . '/import.php';
        include_once dirname(__FILE__) . '/plugin-required.php';
        include_once dirname(__FILE__) . '/options.php';

    }

    public function register_scripts()
    {   
        wp_enqueue_style('font-awesome', get_template_directory_uri() . '/assets/css/font-awesome.min.css', '4.7.0');
        wp_enqueue_style('retheme-admin-dashboard', get_template_directory_uri() . '/core/admin/assets/css/retheme-admin-dashboard.css');
        wp_enqueue_style('retheme-admin-style', get_template_directory_uri() . '/core/admin/assets/css/retheme-admin.css');
        wp_enqueue_script('retheme-admin-scripts', get_template_directory_uri() . '/core/admin/assets/js/retheme-admin.js', array('jquery'), '1.0.0', true);
    }
}

new Admin();
