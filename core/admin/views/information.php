<div class="panel bg-light">

    <div class="panel-heading">
        Informasi
    </div>

    <div class="panel-block">

        <table class="table is-fullwidth">
            <tbody>
                <tr>
                    <td>Versi sekarang</td>
                    <td><?php echo rt_var('product-version') ?></td>
                </tr>
                    <tr>
                    <td>Panduan</td>
                    <td><a href="<?php echo rt_var('product-docs') ?>" target="_blank"><?php echo rt_var('product-docs') ?></a></td>
                </tr>
            </tbody>
        </table>

    </div>

</div>