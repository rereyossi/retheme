<?php  
$theme_name = rt_var('product-name');
$theme_version = rt_var('product-version');
$theme_docs = rt_var('product-docs');
$theme_support = rt_var('product-support');
$theme_group = rt_var('product-group');
?>
<div class="bulma retheme-panel">
    <section class="page-header">
        <div class="page-header-branding">
            <h1 class="title"><?php echo __('Welcome To ', 'rt_domain') . $theme_name ?></h1>
        </div>
        <div class="page-header-version">
            <h2 class="subtitle"><?php echo __('Version ', 'rt_domain') . $theme_version ?></h2>
        </div>
    </section>
    <section class="page-info">
        <div class="columns">

            <div class="column">
                <div class="rta-info-box">
                    <div class="rta-info-box__icon">
                        <i class="fa fa-user"></i>
                    </div>
                    <div class="rta-info-box__body">
                        <h3 class="rta-info-box__title"><?php _e('Need Some Help?', 'rt_domain')?></h3>
                        <p class="rta-info-box__content"><?php _e('We would love to be of any assistance.', 'rt_domain')?></p>
                    </div>
                    <div class="rta-info-box__footer">
                        <a href="<?php echo $theme_support?>" target="_blank" class="button is-info"> <?php _e('Send Ticket', 'rt_domain')?> </a>
                    </div>
                </div>
            </div>

            <div class="column">
                <div class="rta-info-box">
                    <div class="rta-info-box__icon">
                        <i class="fa fa-book"></i>
                    </div>
                    <div class="rta-info-box__body">
                        <h3 class="rta-info-box__title"><?php _e('Documentation', 'rt_domain')?></h3>
                        <p class="rta-info-box__content"><?php _e('Learn about any aspect of ' . $theme_name . ' Theme.', 'rt_domain')?></p>
                    </div>
                    <div class="rta-info-box__footer">
                        <a href="<?php echo $theme_docs ?>" target="_blank" class="button is-info"> <?php _e('Start Reading', 'rt_domain')?> </a>
                    </div>
                </div>
            </div>

             <div class="column">
                <div class="rta-info-box">
                    <div class="rta-info-box__icon">
                        <i class="fa fa-facebook-f"></i>
                    </div>
                    <div class="rta-info-box__body">
                        <h3 class="rta-info-box__title"><?php _e('Facebook Group', 'rt_domain')?></h3>
                        <p class="rta-info-box__content"><?php _e('Disqus and Share of ' . $theme_name . ' Theme.', 'rt_domain')?></p>
                    </div>
                    <div class="rta-info-box__footer">
                        <a href="<?php echo $theme_group ?>" target="_blank" class="button is-info"> <?php _e('Join Group', 'rt_domain')?> </a>
                    </div>
                </div>
            </div>

            <div class="column">
                <div class="rta-info-box">
                    <div class="rta-info-box__icon">
                        <i class="fa fa-facebook-f"></i>
                    </div>
                    <div class="rta-info-box__body">
                        <h3 class="rta-info-box__title"><?php _e('Facebook Group', 'rt_domain')?></h3>
                        <p class="rta-info-box__content"><?php _e('Disqus and Share of ' . $theme_name . ' Theme.', 'rt_domain')?></p>
                    </div>
                    <div class="rta-info-box__footer">
                        <a href="<?php echo $theme_group ?>" target="_blank" class="button is-info"> <?php _e('Join Group', 'rt_domain')?> </a>
                    </div>
                </div>
            </div>

        </div>
    </section>
</div>