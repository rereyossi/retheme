<?php
namespace Retheme\Elementor;

use Elementor\Controls_Manager;
use Retheme\Elementor_Base;
use Elementor\Group_Control_Typography;


if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class Menu extends Elementor_Base
{

    public function get_name()
    {
        return 'retheme-menu';
    }

    public function get_title()
    {
        return __('Theme Menu', 'rt_domain');
    }

    public function get_icon()
    {
        return 'eicon-nav-menu';

    }

    public function get_categories()
    {
        return ['retheme-elements'];
    }

    protected function _register_controls()
    {
        $this->setting_options();
        $this->style_general();
    }

    private function get_available_menus()
    {
        $menus = wp_get_nav_menus();

        $options = [];

        foreach ($menus as $menu) {
            $options[$menu->slug] = $menu->name;
        }

        return $options;
    }

    protected function setting_options()
    {
        $this->start_controls_section(
            'setting_option',
            [
                'label' => __('Options', 'rt_domain'),
            ]
        );

        $menus = $this->get_available_menus();

        if (!empty($menus)) {
            $this->add_control(
                'menu',
                [
                    'label' => __('Menu', 'rt_domain'),
                    'type' => Controls_Manager::SELECT,
                    'options' => $menus,
                    'default' => array_keys($menus)[0],
                    'save_default' => true,
                    'separator' => 'after',
                    'description' => sprintf(__('Go to the <a href="%s" target="_blank">Menus screen</a> to manage your menus.', 'rt_domain'), admin_url('nav-menus.php')),
                ]
            );
        } else {
            $this->add_control(
                'menu',
                [
                    'type' => Controls_Manager::RAW_HTML,
                    'raw' => sprintf(__('<strong>There are no menus in your site.</strong><br>Go to the <a href="%s" target="_blank">Menus screen</a> to create one.', 'rt_domain'), admin_url('nav-menus.php?action=edit&menu=0')),
                    'separator' => 'after',
                    'content_classes' => 'elementor-panel-alert elementor-panel-alert-info',
                ]
            );
        }

        $this->add_control(
            'layout',
            [
                'label' => __('Layout', 'rt_domain'),
                'type' => Controls_Manager::SELECT,
                'default' => 'horizontal',
                'options' => [
                    'horizontal' => __('Horizontal', 'rt_domain'),
                    'vertical' => __('Vertical', 'rt_domain'),
                ],
                'frontend_available' => true,
            ]
        );

        $this->end_controls_section();
    }

    public function style_general()
    {
         $this->start_controls_section(
            'style_general',
            [
                'label' => __('General', 'rt_domain'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );


        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'menu_typography',
                'selector' => '{{WRAPPER}} .rt-menu',
            ]
        );

        $this->start_controls_tabs('tabs_menu_item_style');

        $this->start_controls_tab(
            'tab_menu_item_normal',
            [
                'label' => __('Normal', 'elementor-pro'),
            ]
        );
        $this->add_control(
            'menu_color',
            [
                'label' => __('Color', 'elementor-pro'),
                'type' => Controls_Manager::COLOR,
                'default' => '',
                'selectors' => [
                    '{{WRAPPER}} .rt-menu__item a' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'tab_menu_item_hover',
            [
                'label' => __('Hover', 'elementor-pro'),
            ]
        );
        $this->add_control(
            'menu_color_hover',
            [
                'label' => __('Color', 'elementor-pro'),
                'type' => Controls_Manager::COLOR,
                'default' => '',
                'selectors' => [
                    '{{WRAPPER}} .rt-menu__item a:hover' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'tab_menu_item_active',
            [
                'label' => __('Active', 'elementor-pro'),
            ]
        );
        $this->add_control(
            'menu_color_active',
            [
                'label' => __('Color', 'elementor-pro'),
                'type' => Controls_Manager::COLOR,
                'default' => '',
                'selectors' => [
                    '{{WRAPPER}} .current-menu-item .rt-menu__item a' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->end_controls_tab();

        $this->end_controls_tabs();

        $this->add_control(
            'hr',
            [
                'type' => Controls_Manager::DIVIDER,
                'style' => 'thick',
            ]
        );

        $this->add_responsive_control(
            'menu_gap',
            [
                'label' => __('Space Between', 'rt_domain'),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .rt-menu--simple-horizontal .rt-menu__item' => 'padding: 0 {{SIZE}}{{UNIT}} !important',
                     '{{WRAPPER}} .rt-menu--simple-vertical .rt-menu__item' => 'padding: {{SIZE}}{{UNIT}} 0 !important',
                ],
            ]

        );

        $this->end_controls_section();


    }

    protected function render()
    {
        $settings = $this->get_settings();

        include dirname(__FILE__) . '/menu-view.php';

    }
    /* end class */
}
