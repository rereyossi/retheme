<?php
namespace Retheme\Elementor;

use Retheme\Elementor_Base;
use Retheme\Helper;
use Retheme\HTML;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Background;
use Elementor\Group_Control_Typography;

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class Carousel extends Elementor_Base
{
    public function get_name()
    {
        return 'retheme-carousel';
    }

    public function get_title()
    {
        return __('Image Carousel', 'rt_domain');
    }

    public function get_icon()
    {
        return 'ate-icon ate-post-tiles';

    }

    public function get_categories()
    {
        return ['retheme-elements'];
    }

    protected function _register_controls()
    {
        $this->setting_options(); //protected
        $this->setting_content();

        // extend retheme base
        $this->setting_carousel(array(
            'carousel' => true,
        ));
    }

    protected function setting_options()
    {
        $this->start_controls_section(
            'setting_option',
            [
                'label' => __('Options', 'rt_domain'),
            ]
        );


        $this->add_control(
            'image_size',
            [
                'label' => __('Image Size', 'rt_domain'),
                'type' => Controls_Manager::SELECT,
                'default' => 'large',
                'options' => Helper::get_image_size(),
            ]
        );


        $this->end_controls_section();
    }

    public function setting_content()
    {
        $this->start_controls_section(
            'setting_content',
            [
                'label' => __('Content', 'rt_domain'),
            ]
        );

        $repeater = new \Elementor\Repeater();

        $repeater->add_control(
            'image',
            [
                'label' => __('Choose Image', 'rt_domain'),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => \Elementor\Utils::get_placeholder_image_src(),
                ],
            ]
        );

        $repeater->add_control(
            'title', [
                'label' => __('Title', 'plugin-domain'),
                'type' => Controls_Manager::TEXT,
                'default' => __('Title', 'plugin-domain'),
            ]
        );


        $repeater->add_control(
            'link',
            [
                'label' => __('url', 'plugin-domain'),
                'type' => Controls_Manager::URL,
                'placeholder' => __('https://your-link.com', 'plugin-domain'),
                'show_external' => true,
                'label_block' => false,
                'default' => [
                    'url' => '',
                    'is_external' => false,
                    'nofollow' => true,
                ],
            ]
        );

        $this->add_control(
            'sliders',
            [
                'label' => __('iTEMS', 'plugin-domain'),
                'type' => Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
                'default' => [
                    [
                        'title' => __('Slide #1', 'plugin-domain'),
                    ],
                ],
                'title_field' => '{{{ title }}}',
            ]
        );

        $this->end_controls_section();

    }



    protected function render()
    {
        $settings = $this->get_settings();
        $classes[] = 'rt-slider js-elementor-slider';

        echo HTML::before_slider(array(
            'id' => 'rt-slider-' . $this->get_id(),
            'class' => $classes,
            'items-lg' => $settings['slider_item'],
            'items-md' => $settings['slider_item_tablet'],
            'items-sm' => $settings['slider_item_mobile'],
            'pagination' => $settings['slider_pagination'],
            'gap' => $settings['slider_gap'],
            'nav' => $settings['slider_nav'],
        ));

        echo HTML::open('rt-slider__main owl-carousel');

        foreach ($settings['sliders'] as $key => $slider) {
            include dirname(__FILE__) . '/carousel-view.php';
        }

        echo HTML::close();

        echo HTML::after_slider();

    }
}
