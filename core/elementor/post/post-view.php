<?php
$date_url = esc_url(get_day_link(get_the_time('Y'), get_the_time('m'), get_the_time('d')));
$author_url = get_author_posts_url(get_the_author_meta('ID'));
$avatar = get_avatar(get_the_author_meta('ID'), 30);
$avatar_url = get_the_author_meta('profile_avatar', get_the_author_meta('ID'));
?>

	<div class="flex-item">
	
		<div id="<?php echo 'post-' . get_the_ID() ?>" <?php post_class('rt-post rt-post--grid')?>>


		<div class="rt-post__thumbnail rt-img rt-img--full">
			<?php the_post_thumbnail($settings['image_size'])?>
		</div>

		<div class="rt-post__body">

			<?php if (get_the_category() && $settings['meta_category']): ?>
				<div class="rt-badges <?php echo $the_cat ?>">
					<?php foreach (get_the_category() as $term): ?>
					<a href="<?php echo get_category_link($term->term_id) ?>" class="<?php echo $term->slug ?>" ><?php echo $term->name ?></a>
					<?php endforeach?>
				</div>
			<?php endif?>

			<h3 class="rt-post__title"><a href="<?php the_permalink()?>"><?php the_title()?></a></h3>

			
			<div class="rt-post__meta">

				<?php if ($settings['meta_date'] === 'yes'): ?>
				<a class="rt-post__meta-item date" href="<?php echo $date_url ?>"><i class="fa fa-calendar"></i><?php echo get_the_date() ?></a>
				<?php endif; ?>

				<?php if ($settings['meta_author'] === 'yes'): ?>
				<a class="rt-post__meta-item author" href="<?php echo $author_url ?>"><i class="fa fa-user"></i><?php the_author()?></a>
				<?php endif; ?>

				<?php if ($settings['meta_comment']  === 'yes' && get_comments_number() >= 1): ?>
				<span class="rt-post__meta-item comment"><i class="fa fa-comment"></i><?php echo get_comments_number() ?></span>
				<?php endif?>

			</div>

			<?php if (!empty($settings['excerpt'])): ?>
				<div class="rt-post__content">
					<?php echo rt_the_content($settings['excerpt']) ?>
				</div>
			<?php endif?>

			<?php if (!empty($settings['readmore'])): ?>
			<a href="<?php echo get_permalink() ?>" class="rt-post__readmore"><?php echo $settings['readmore'] ?></a>
			<?php endif?>

		</div>



		</div>
	</div>