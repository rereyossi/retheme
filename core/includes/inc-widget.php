<?php
/*=================================================;
/* SET SIZE TAG
/*================================================= */
function rt_tag_cloud_widget($args)
{
    $args['largest']  = 12;
    $args['smallest'] = 12;
    $args['unit']     = 'px';

    return $args;
}
add_filter('widget_tag_cloud_args', 'rt_tag_cloud_widget');

/*=================================================
*  CATEGORY ADD SPAN
/*================================================= */
// function rt_span_cat_count($links)
// {
//     $links = str_replace('(', '<span class="cat_count">(', $links);
//     $links = str_replace(')', ')</span>', $links);

//     return $links;
// }
// add_filter('wp_list_categories', 'rt_span_cat_count');


