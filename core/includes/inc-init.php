<?php
include_once dirname(__FILE__) . '/inc-setup.php';
include_once dirname(__FILE__) . '/inc-option.php';
include_once dirname(__FILE__) . '/inc-helper.php';
include_once dirname(__FILE__) . '/inc-image.php';
include_once dirname(__FILE__) . '/inc-template-part.php';
include_once dirname(__FILE__) . '/inc-template-tag.php';

include_once dirname(__FILE__) . '/inc-style.php';
include_once dirname(__FILE__) . '/inc-script.php';

include_once dirname(__FILE__) . '/inc-post.php';
include_once dirname(__FILE__) . '/inc-comment.php';

include_once dirname(__FILE__) . '/inc-widget.php';
include_once dirname(__FILE__) . '/inc-menu.php';

if(rt_is_woocommerce()){
    include_once dirname(__FILE__) . '/inc-woocommerce.php';
}



