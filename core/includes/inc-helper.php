<?php

/*=================================================;
/* TEMPLATE PART
/*================================================= */
function rt_get_template_part($slug, $name = '', $variable = '')
{
    get_template_part('template-parts/' . $slug, $name);
}

/*=================================================
 * GET FONTAWESOME 5 CLASSES
/*================================================= */
function rt_get_fontawesome_class($name)
{

    $social = array(
        'facebook' => 'facebook-f',
        'vimeo-v' => 'vimeo-v',
        'pinterest' => 'pinterest-p',
        'telegram' => 'telegram-plane',
        'medium' => 'medium-m',
    );

    /** return value by array */
    $data = !empty($social[$name])?$social[$name]: $name;

    return 'fa fa-' . $data;

}

/*=================================================
 *  LIMIT STRING
/*================================================= */
function rt_limited_string($the_excerpt, $excerpt_length)
{
    $the_excerpt = strip_tags(strip_shortcodes($the_excerpt));
    $words = explode(' ', $the_excerpt, $excerpt_length + 1);

    if (count($words) > $excerpt_length):
        array_pop($words);
        $the_excerpt = implode(' ', $words);
        $the_excerpt .= '...';
    endif;

    return $the_excerpt;
}

/*=================================================;
/* SET CLASS ROW
/*================================================= */
function rt_set_class_raw($filter = '', $classes = array())
{
    if (!empty($filter)) {
        $class_output = join(' ', apply_filters($filter, array_unique($classes)));
    } else {
        $class_output = join(' ', array_unique($classes));
    }

   return 'class="' . $class_output . '"';
}

/*=================================================
 *  SET CLASS
/*================================================= */
function rt_set_class($filter = '', $classes = array())
{
    echo rt_set_class_raw($filter, $classes);
}


/*=================================================;
/* HANDLE THEME
/*================================================= */
function rt_is_premium(){
    return (get_option(rt_var('product-slug', '_status')) == 'active')? true: false;
}

function rt_is_lite(){
    return (get_option(rt_var('product-slug', '_status')) == 'deactive')? true: false;
}