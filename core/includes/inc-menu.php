<?php
/*=================================================;
/* MENU
/*================================================= */
/**
 * Add class to menu item
 * @category Menu
 * @param [type] $classes
 * @param [type] $item
 * @return void
 */
function rt_menu_class($classes, $item)
{
    if ($item->current) {
        $classes[] = "is-active";
    }

    $classes[] = "rt-menu__item";

    return $classes;

}
add_filter('nav_menu_css_class', 'rt_menu_class', 10, 2);

/**
 * add class sub menu
 * @category menu
 * @param [type] $menu
 * @return void
 */
function rt_submenu_class($menu)
{
    $menu = preg_replace('/ class="sub-menu"/', '/ class="sub-menu rt-menu__submenu" /', $menu);
    return $menu;
}

add_filter('wp_nav_menu', 'rt_submenu_class');

/*=================================================;
/* SET DEFAULT MENU
/*================================================= */

// Check if the menu exists
$menu_name = 'Main Menu';
$menu_exists = wp_get_nav_menu_object( $menu_name );

// If it doesn't exist, let's create it.
if( empty($menu_exists->slug)){
    $menu_id = wp_create_nav_menu($menu_name);
}

$locations = get_theme_mod('nav_menu_locations');
$location['primary'] = 'Main Menu';
set_theme_mod('nav_menu_locations', $location);

