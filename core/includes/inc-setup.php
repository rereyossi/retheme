<?php
/**
 * retheme functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package retheme
 */

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function retheme_setup()
{
    /*
     * Make theme available for translation.
     * Translations can be filed in the /languages/ directory.
     * If you're building a theme based on retheme, use a find and replace
     * to change 'retheme' to the name of your theme in all the template files.
     */
    load_theme_textdomain('rt_domain', get_template_directory() . '/languages');
    

    // Add default posts and comments RSS feed links to head.
    add_theme_support('automatic-feed-links');

    //Turn on wide images
    add_theme_support('align-wide');

    /*
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
    add_theme_support('title-tag');

    /*
     * Enable support for Post Thumbnails on posts and pages.
     *
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('post-thumbnails');

    // This theme uses wp_nav_menu() in one location.
    register_nav_menus(array(
        'primary' => 'Menu Primary',
        'second' => 'Menu Second',
        'thirty' => 'Menu Thirty',
        'topbar' => 'Menu Topbar',
        'footer' => 'Menu Footer',
    ));

    /*
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
    add_theme_support('html5', array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    ));

    /**
     * Add theme support selective refresh for customizer
     */
    add_theme_support('customize-selective-refresh-widgets');

    /** IMAGE SIZE */
    add_image_size('featured_medium', 400, 225, true);
    add_image_size('featured_medium_nocrop', 400, 999999);


    add_theme_support('title-tag');
    

    if (!isset($content_width)) {
        $content_width = 1170;
    }

    do_action('rt_load_theme');

}
add_action('after_setup_theme', 'retheme_setup');




/*=================================================;
/* DEFAUT VARIABLE
/*================================================= */
function rt_var($value, $arg = '')
{
    $default = apply_filters('rt_var', array(
        'product-name' => 'Retheme',
        'product-slug' => 'retheme',
        'product-version' => '1.3.33',
        'product-docs' => 'https://www.webforia.id/docs/retheme',
        'product-group' => 'https://web.facebook.com/groups/265676900996871',
        'product-support' => 'https://web.facebook.com/groups/265676900996871',
        'font-primary' => 'Roboto',
        'font-weight' => '500',
        'font-second' => 'Roboto',
        'color-primary' => '#222222',
        'color-second' => '#333333',
        'font-size-body' => '14px',
        'line-height-body' => '1.5',
        'search-style' => 'dropdown',
    )
    );

    return !empty($default[$value]) ? $default[$value].$arg : '';
}
