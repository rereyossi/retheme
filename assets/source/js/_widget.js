jQuery(document).ready(function($) {
  var list_dropdown = function() {
    var main_element = $(
      ".product-categories, .widget_categories ul, .widget_nav_menu ul"
    );

    main_element.each(function() {
      var element = $(this);

      // added arrow trigger each submenu
      element.find("li").each(function() {
        if ($(this).find("ul").length > 0) {
          $(this)
            .children("a")
            .after(
              "<span class='rt-list__arrow'><i class='fa fa-angle-down'></i></span>"
            );
        }
      });
      // action dropdown
      element.find("li").on("click", "> .rt-list__arrow", function(event) {
        event.preventDefault();

        if ($(this).hasClass("is-active")) {
          $(this).removeClass("is-active");
          $(this)
            .parent()
            .children("ul")
            .velocity("slideUp");
        } else {
          element.find("ul").velocity("slideUp");
          $(".rt-list__arrow").removeClass("is-active");

          $(this).addClass("is-active");
          $(this)
            .parent()
            .children("ul")
            .velocity("slideDown");
        }
      });
    });
  };

  list_dropdown();

  /*=================================================;
  /* MENU HEADER
  /*================================================= */
  var menu_widget_dropdown = function() {
    var selector = {
      element: ".widget_nav_menu.rt-widget--footer",
      body: ".menu"
    };

    var main_element = $(selector.element);

    $(selector.element).find(selector.body).css("display", "none");

    main_element.each(function() {
      var element = $(this);

      // added arrow trigger each submenu
      element.each(function() {
        $(this)
          .find(".rt-widget__title")
          .after(
            "<span class='rt-widget__arrow'><i class='fa fa-chevron-down'></i></span>"
          );
      });

      // action dropdown
      element.find(".rt-widget__arrow").on("click", function(event) {
        event.preventDefault();

        if ($(this).hasClass("is-active")) {
          $(this).removeClass("is-active");
          $(this)
            .parents(selector.element)
            .find(selector.body)
            .velocity("slideUp");
        } else {
          main_element.find(selector.body).velocity("slideUp");

          main_element.find(".rt-widget__arrow").removeClass("is-active");

          $(this).addClass("is-active");
          $(this)
            .parents(selector.element)
            .find(selector.body)
            .velocity("slideDown");
        }
      });
    });
  };

  var windows = $(window).width();

  if (windows < 576) {
    menu_widget_dropdown();
  }
});
