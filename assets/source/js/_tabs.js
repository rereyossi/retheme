
jQuery(document).ready(function($) {

  $.fn.retheme_tabs = function(options) {
      return this.each(function() {
          var element = $(this);

          var defaults = {
              animatein: 'transition.fadeIn',
              animateout: 'transition.fadeOut',
              duration: '300',
              easing: 'easeIn',
          };

          var meta = element.data();
          var options = $.extend(defaults, options, meta);

          var selector = {
              nav: '.rt-tab__title',
              item: '.rt-tab__item',
          }
          
          /*
           * Event
           */
          element.find(selector.nav).on('click', function(event) {
              event.preventDefault();

              var tab_item = $(this).attr("href");

              /*
               * Close all Tabs
               */
              element.find(selector.nav).removeClass('is-active');
              element.find(selector.item).hide().removeClass('is-active');


    
              /** active tab menu */
              $(this).addClass('is-active');

            /** Open tab content */
              $(this).parents('.rt-tab').find(tab_item)
                  .velocity(options.animatein, {
                      duration: options.duration,
                      easing: options.easing
                  }).addClass('is-active');


          });
      });

  };

});
