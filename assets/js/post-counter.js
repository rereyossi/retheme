/*=================================================
*  AJAX CALL
// =================================================== */

jQuery(document).ready(function ($) {

    $(window).on('load', function(){
        var element = $('.js-post-counter');
        var data = {
            'action': 'ajax_post_count_result',
            'query': post_counter.posts,
            'post_id': element.attr('id'),
        };

        jQuery.ajax({
            url: post_counter.ajaxurl,
            type: 'post',
            dataType: 'html',
            data: data,
            success: function (response) {
                element.text(response);
            }
        });
    });

});
