jQuery(document).ready(function ($) {
    
    /*=================================================;
    /* GALLERY CAROUSEL
    /*================================================= */
    $(".flex-control-thumbs").addClass("rt-slider owl-carousel");
    $(".flex-control-thumbs").owlCarousel({
      loop: false,
      autoplay: false,
      margin: 10,
      dots: false,
      nav: true,
      navText: [
        "<i class='ti-angle-left'></i>",
        "<i class='ti-angle-right'></i>"
      ],
      responsive: {
        0: {
          items: 5
        },
        720: {
          items: 5
        },
        960: {
          items: 5
        }
      }
    });

});
