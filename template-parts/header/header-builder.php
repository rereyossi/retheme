<?php
/**
 * Header - Aligned (Default)
 */
$classes[] = 'rt-header js-header';
$classes[] = !empty(rt_get_field('header_overlay'))? 'is-overlay': '';

?>
<div <?php rt_set_class('rt_header_class', $classes) ?> data-animatein='<?php echo rt_option('submenu_animatein', 'transition.fadeIn') ?>' data-animateout='<?php echo rt_option('submenu_animateout', 'transition.fadeOut') ?>' data-duration='<?php echo rt_option('submenu_duration', '300') ?>' data-sticky='<?php echo apply_filters('rt_header_sticky', rt_option('header_sticky', true)) ?>'  data-responsive='<?php echo rt_option('header_nav_breakpoint', 900)?>'>
  
  <?php do_action('rt_before_header'); ?>

  <div id="header-topbar" class="rt-header__wrapper">
    <?php rt_get_template_part('header/header-topbar') ?>
  </div>
  <div id="header-middle" class="rt-header__wrapper">
    <?php rt_get_template_part('header/header-middle')?>
  </div>

  <div id="header-main" class="rt-header__wrapper">
    <?php rt_get_template_part('header/header-main') ?>
    <?php rt_get_template_part('header/header-sticky')?>
    <?php rt_get_template_part('header/header-search'); ?>
  </div>

  <?php do_action('rt_after_header'); ?>

</div>


<?php rt_get_template_part('header/header-mobile') ?>

