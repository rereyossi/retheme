<?php if (rt_option('header_html_3')): ?>
  <div id="html-3" class="rt-header__element rt-header__html">

  <?php if (rt_option('header_html_3_shortcode')): ?>
    <?php echo do_shortcode(rt_option('header_html_3')) ?>
  <?php else: ?>

      <?php if(rt_option('header_html_3_icon')):?>
          <i class="<?php echo 'mr-5 fa fa-' . rt_option('header_html_3_icon') ?>"></i>
      <?php endif;?>

      <?php echo rt_option('header_html_3', 'Your Text/HTML') ?>
  <?php endif;?>

  </div>
<?php endif?>