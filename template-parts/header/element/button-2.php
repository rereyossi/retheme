<?php
$target = '';
if (rt_option('header_button_2_link')) {
  $target = 'target="'.rt_option('header_button_2_link_target', 'blank').'"';
}
?>
<div id="button-2" class="rt-header__element rt-header__btn">
  <a href="<?php echo rt_option('header_button_2_link', '#') ?>" class="rt-btn rt-btn--2 mb-0" <?php echo $target  ?>>

    <?php if (rt_option('header_button_2_icon')): ?>
      <i class="<?php echo 'fa fa-'.rt_option('header_button_2_icon') ?>"></i>
    <?php endif; ?>

    <span><?php echo rt_option('header_button_2_text', 'Button 2') ?></span>
  </a>
</div>
