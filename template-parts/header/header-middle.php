<?php if (rt_option('header_middle', false)): ?>

<?php 
$options = rt_option('header_builder_option');
$defaults = array(
  'middle_left_display' => 'normal',
  'middle_left_alignment' => 'left',
  'middle_center_display' => 'grow',
  'middle_center_alignment' => 'left',
  'middle_right_display' => 'normal',
  'middle_right_alignment' => 'left',
);

$elements = wp_parse_args($options, $defaults);
?>
  <?php do_action('rt_before_header_middle')?>

  <div id="header-middle" class="rt-header__middle">
      <div class="page-container">

        <div id="header-middle-left" class="rt-header__column" data-alignment="<?php echo $elements['middle_left_alignment'] ?>" data-display="<?php echo $elements['middle_left_display'] ?>">
          <?php do_action('rt_header_middle_left')?>
        </div>
        
        <div id="header-middle-center" class="rt-header__column" data-alignment="<?php echo $elements['middle_center_alignment']?>" data-display="<?php echo $elements['middle_center_display']?>">
            <?php do_action('rt_header_middle_center')?>
        </div>

        <div id="header-middle-right" class="rt-header__column" data-alignment="<?php echo $elements['middle_right_alignment']?>" data-display="<?php echo $elements['middle_right_display'] ?>">
            <?php do_action('rt_header_middle_right')?>
        </div>

      </div>
  </div>

  <?php do_action('rt_after_header_middle')?>
  
<?php endif; ?>
