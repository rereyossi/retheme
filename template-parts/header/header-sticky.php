<?php if (rt_option('header_sticky', true)): ?>

<?php 
/** Options */
$values = rt_option('header_builder_option');
$defaults = array(
  'sticky_left_display' => 'normal',
  'sticky_left_alignment' => 'left',
  'sticky_center_display' => 'grow',
  'sticky_center_alignment' => 'left',
  'sticky_right_display' => 'normal',
  'sticky_right_alignment' => 'left',
);

$elements = wp_parse_args($values, $defaults);
?> 
  
  <div id="header-sticky" class="rt-header__sticky">
      <div class="page-container">

        <div id="header-sticky-left" class="rt-header__column" data-alignment="<?php echo $elements['sticky_left_alignment'] ?>" data-display="<?php echo $elements['sticky_left_display'] ?>">
          <?php  do_action('rt_header_sticky_left')?>
      </div>
      
       <div id="header-sticky-center" class="rt-header__column" data-alignment="<?php echo $elements['sticky_center_alignment'] ?>" data-display="<?php echo $elements['sticky_center_display'] ?>">
         <?php  do_action('rt_header_sticky_center')?>
      </div>

       <div id="header-sticky-right" class="rt-header__column" data-alignment="<?php echo $elements['sticky_right_alignment'] ?>" data-display="<?php echo $elements['sticky_right_display'] ?>">
         <?php  do_action('rt_header_sticky_right')?>
      </div>


      </div>
  </div>
<?php endif; ?>
