<?php

// get all category by current post
$category = join(', ', Retheme\Helper::get_terms('category', get_the_id()));

// default query
$query_default = array(
       'post__not_in'  => array(get_the_ID()),
       'post_type'     => 'post',
       'category_name' => $category,
       'posts_per_page'=> rt_option('single_related_count', 6),
   );

// helper filter query
$query_by = Retheme\Helper::query(array(
  "query_by" => rt_option('single_related_choose'),
  "posts_per_page" => rt_option('posts_per_page'),
));

// merge query arggument
$args =  wp_parse_args($query_by, $query_default);

$the_query = new WP_Query($query_default);
?>

<?php if ($the_query->have_posts()): ?> 
<?php
echo \Retheme\HTML::before_slider(array(
    'id' => 'post-related-slider',
    'class' => ["rt-post-related mb-30 rt-slider js-slider"],
    'items-lg' => rt_option('single_related_show', 3),
    'items-sm' => rt_option('single_related_show_tablet', 3),
    'items-sm' => rt_option('single_related_show_mobile', 2),
    'nav'=> false,
    'pagination' => false,
    'gap' => 10,
));
?>

	<?php echo \Retheme\HTML::header_block(array(
    "title" => __("You also like", "rt_domain"),
    "nav" => true,
)) ?>

  <div class="rt-post-related__body rt-slider__main owl-carousel">

        <?php while ($the_query->have_posts()): ?> 
          <?php $the_query->the_post(); ?> 

          <div class="rt-post">
            <?php rt_post_thumbnail() ?>
            <?php rt_post_title() ?>
            <?php rt_post_meta() ?>
          </div>

        <?php endwhile; ?>

    <?php
     wp_reset_postdata();
   ?>

  </div>

<?php echo \Retheme\HTML::after_slider();?>

<?php endif ?>
