<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.3.2
 */

defined('ABSPATH') || exit;

// Note: `wc_get_gallery_image_html` was added in WC 3.3.2 and did not exist prior. This check protects against theme overrides being used on older versions of WC.
if (!function_exists('wc_get_gallery_image_html')) {
    return;
}

global $product;

$post_thumbnail_id = $product->get_image_id();
$image = wp_get_attachment_image($post_thumbnail_id, 'shop_single', true, array("class" => "attachment-shop_single size-shop_single wp-post-image"));

$wrapper_classes = apply_filters('woocommerce_single_product_image_gallery_classes', array(
    'rt-slider__main',
    'owl-carousel',
    'rt-slider--pagination-inner',
    'rt-single-product--' . (has_post_thumbnail() ? 'with-images' : 'without-images'),
    'images',

));

?>
<?php
echo \Retheme\HTML::before_slider(array(
    'id' => 'product-single-gallery',
    'class' => ["rt-slider js-slider mb-30"],
    'items-lg' => 1,
    'items-sm' => 1,
    'items-sm' => 1,
    'nav'=> false,
    'pagination' => true,
    'gap' => 10,
    'autoplay' => false,
));
?>

<div class="<?php echo esc_attr(implode(' ', array_map('sanitize_html_class', $wrapper_classes))); ?>" style="float: none; width: 100%;">
		<?php

        if (has_post_thumbnail()) {
            $attachment_ids = $product->get_gallery_image_ids();

            $lightbox_src = wc_get_product_attachment_props($post_thumbnail_id);

            echo '<div class="woocommerce-product-gallery__image single-product-main-image"><a class="wc-gallery-box"  title="' . $lightbox_src['title'] . '" data-gall="single-product-lightbox" href="' . $lightbox_src['url'] . '" >' . $image . '</a></div> ';

            if ($attachment_ids) {
                foreach ($attachment_ids as $attachment_id) {
                    $thumbnail_image = wp_get_attachment_image($attachment_id, 'shop_single');
                    $lightbox_src = wc_get_product_attachment_props($attachment_id);

                
                    echo '<a class="wc-gallery-box" data-gall="single-product-lightbox" title="' . $lightbox_src['title'] . '" >' . $thumbnail_image . '</a>';

                }
            }
        } else {
            $html = '<div class="woocommerce-product-gallery__image--placeholder">';
            $html .= sprintf('<img src="%s" alt="%s" class="wp-post-image" />', esc_url(wc_placeholder_img_src()), esc_html__('Awaiting product image', 'woocommerce'));
            $html .= '</div>';
        }
?>
</div>

<?php echo \Retheme\HTML::after_slider();?>

